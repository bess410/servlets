package com.epam.andrei_sterkhov;

import com.epam.andrei_sterkhov.data_base.DataBase;
import com.epam.andrei_sterkhov.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        req.getRequestDispatcher("jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        user.setLogin(req.getParameter("login"));
        user.setUserName(req.getParameter("user_name"));
        user.setSurname(req.getParameter("surname"));
        user.setPass(req.getParameter("pass"));

        DataBase.getUserList().add(user);

        HttpSession session = req.getSession(false);
        session.setAttribute("currentUser", user);
        resp.sendRedirect("index");
    }
}

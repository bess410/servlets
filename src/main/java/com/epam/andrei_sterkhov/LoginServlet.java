package com.epam.andrei_sterkhov;

import com.epam.andrei_sterkhov.data_base.DataBase;
import com.epam.andrei_sterkhov.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        req.getRequestDispatcher("jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");
        User currentUser = null;

        for (User user : DataBase.getUserList()) {
            if (user.getLogin().equals(login) && (user.getPass().equals(pass))) {
                currentUser = user;
            }
        }

        if(currentUser != null){
            HttpSession session = req.getSession(false);
            session.setAttribute("currentUser", currentUser);
            resp.sendRedirect("index");
        } else {
            req.getRequestDispatcher("jsp/login.jsp").forward(req, resp);
        }
    }
}

package com.epam.andrei_sterkhov.enums;

public enum Category {
    CAT1("Категория 1"), CAT2("Категория 2"), CAT3("Категория 3");
    private String name;

    Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

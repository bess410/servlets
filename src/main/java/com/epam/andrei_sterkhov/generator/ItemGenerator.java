package com.epam.andrei_sterkhov.generator;

import com.epam.andrei_sterkhov.enums.Category;
import com.epam.andrei_sterkhov.dto.Item;

import java.util.*;

public class ItemGenerator {
    public List<Item> getItemList(){
        List<Item> itemList = new ArrayList<>();
        Item item;
        int price;
        String[] names = {"Thing", "Another", "Hrenovina", "Same thing"};
        String name;
        Random random = new Random();
        for (int i = 0; i < 36; i++) {
            price = (random.nextInt(10) + 1) * 1000;
            name = names[random.nextInt(names.length)];
            item = new Item(name, price, "Description", "../images/maifun.jpg",
                    Category.values()[random.nextInt(3)]);
            itemList.add(item);
        }
        itemList.sort(Comparator.comparing(o -> o.getCategory().getName()));
        return itemList;
    }
}

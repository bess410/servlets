package com.epam.andrei_sterkhov.data_base;

import com.epam.andrei_sterkhov.dto.User;

import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private static List<User> userList = new ArrayList<>();

    private DataBase() {
    }

    public static List<User> getUserList() {
        return userList;
    }
}

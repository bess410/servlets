package com.epam.andrei_sterkhov;

import com.epam.andrei_sterkhov.dto.Item;
import com.epam.andrei_sterkhov.dto.User;
import com.epam.andrei_sterkhov.generator.ItemGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/index")
public class HomeServlet extends HttpServlet {
    private final ItemGenerator generator = new ItemGenerator();
    private final List<Item> itemList = generator.getItemList();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("currentUser");
        if (user != null) {
            getServletContext().setAttribute("currentUser", user);
            getServletContext().setAttribute("itemList", itemList);
            req.getRequestDispatcher("jsp/index.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("login");
        }
    }
}
